FROM openjdk:14-alpine
COPY build/libs/azure-function-context-path-issue-*-all.jar azure-function-context-path-issue.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "azure-function-context-path-issue.jar"]