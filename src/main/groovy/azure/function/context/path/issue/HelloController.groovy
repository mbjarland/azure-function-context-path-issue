package azure.function.context.path.issue

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller("/hello")
class HelloController {

    @Get(uri="/", produces="text/plain")
    String index() {
        "Example Response"
    }
}